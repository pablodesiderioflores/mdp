//arrays globales de mensajes
  var ots      = [];
  var logins   = [];
  var logouts  = [];
  var mensajes = [];
//variable global base de datos SQLite
  var db = null;
//variable global para la url del api
  var dominiourl;
//variable global patente del camion
  var patente;
//variable global lectura de mensaje
  var lectura = false;
//Constantes para guardar el tipo de mensaje en SQLite
  var LOGIN_CONST   = 1;
  var LOGOUT_CONST  = 2;
  var OT_CONST      = 3;
  var MSJ_CONST     = 4;
//constantes estados leidos no leidos
  
  var NO_LEIDO = 0;
  var LEIDO    = 1;
//Variable bandera para ejecutar una accion solo cuando se carga la app
  var flagSQliteData;
//variable para contar las Ordenes de trabajo no leidas y mostrarlas como notificacion
  var contOts = 0;
//variable para contar los Mensajes no leidos y mostrarlos como notificacion
  var contMsgs = 0;
//variable para contar los mensajes de login no leidos y mostrarlos como notificacion
  var contLogins = 0;
//variable para contar los Mensajes de logout no leidos y mostrarlos como notificacion
  var contLogouts = 0;
  
//Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ngCordova' ,'starter.controllers', 'starter.services'])

.run(function($ionicPlatform, $cordovaSQLite,$http,$location,$ionicLoading,$timeout,$state) {
  flagSQliteData = true;
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
         if (window.cordova)
         {
            db = window.sqlitePlugin.openDatabase({ name: "mdp.sqlite", androidDatabaseImplementation: 2, androidLockWorkaround: 1}); //device
            db.transaction(function(tx) {
              tx.executeSql("CREATE TABLE IF NOT EXISTS user (idusuario INTEGER PRIMARY KEY AUTOINCREMENT, usuario VARCHAR, password VARCHAR);");
              tx.executeSql("CREATE TABLE IF NOT EXISTS movil (idmovil INTEGER PRIMARY KEY AUTOINCREMENT, dominiourl VARCHAR, patente VARCHAR);");
              tx.executeSql("CREATE TABLE IF NOT EXISTS mensajeria (idmensaje INTEGER PRIMARY KEY AUTOINCREMENT, idtipomensaje INTEGER, idestado INTEGER, mensaje VARCHAR, fecha DATE);");
              tx.executeSql("CREATE TABLE IF NOT EXISTS estado (idestado INTEGER PRIMARY KEY AUTOINCREMENT, estado INTEGER);");
              tx.executeSql("CREATE TABLE IF NOT EXISTS tipomensaje (idtipomensaje INTEGER PRIMARY KEY AUTOINCREMENT, descripcion VARCHAR);");
              tx.executeSql("INSERT INTO user (usuario,password) values ('useradmin','3asy7rack4dm');");
              tx.executeSql("INSERT INTO estado (idestado, estado) values (0,'NO LEIDO');");
              tx.executeSql("INSERT INTO estado (idestado, estado) values (1,'LEIDO');");
              tx.executeSql("INSERT INTO tipomensaje (descripcion) values ('login');");
              tx.executeSql("INSERT INTO tipomensaje (descripcion) values ('logout');");
              tx.executeSql("INSERT INTO tipomensaje (descripcion) values ('ot');");
              tx.executeSql("INSERT INTO tipomensaje (descripcion) values ('mensaje');");
            });
        }else{
            db = window.openDatabase("mdp.sqlite", '1', 'mdp', 1024 * 1024 * 100); // browser
            db.transaction(function(tx) {
              tx.executeSql("CREATE TABLE IF NOT EXISTS user (idusuario INTEGER PRIMARY KEY AUTOINCREMENT, usuario VARCHAR, password VARCHAR);");
              tx.executeSql("CREATE TABLE IF NOT EXISTS movil (idmovil INTEGER PRIMARY KEY AUTOINCREMENT, dominiourl VARCHAR, patente VARCHAR);");
              tx.executeSql("CREATE TABLE IF NOT EXISTS mensajeria (idmensaje INTEGER PRIMARY KEY AUTOINCREMENT, idtipomensaje INTEGER, idestado INTEGER, mensaje VARCHAR, fecha DATE);");
              tx.executeSql("CREATE TABLE IF NOT EXISTS estado (idestado INTEGER PRIMARY KEY, estado INTEGER);");
              tx.executeSql("CREATE TABLE IF NOT EXISTS tipomensaje (idtipomensaje INTEGER PRIMARY KEY AUTOINCREMENT, descripcion VARCHAR);");
              tx.executeSql("INSERT INTO user (usuario,password) values ('useradmin','3asy7rack4dm');");
              tx.executeSql("INSERT INTO estado (idestado, estado) values (0,'NO LEIDO');");
              tx.executeSql("INSERT INTO estado (idestado, estado) values (1,'LEIDO');");
              tx.executeSql("INSERT INTO tipomensaje (descripcion) values ('login');");
              tx.executeSql("INSERT INTO tipomensaje (descripcion) values ('logout');");
              tx.executeSql("INSERT INTO tipomensaje (descripcion) values ('ot');");
              tx.executeSql("INSERT INTO tipomensaje (descripcion) values ('mensaje');");
            });
        }
          db.transaction(function(tx) {
          var sql = "SELECT * FROM movil";
          tx.executeSql(sql, [], function(tx, res)
          {
              if(res.rows.length > 0 )
              {
                dominiourl = res.rows.item(0).dominiourl;
                patente    = res.rows.item(0).patente;
                console.log(dominiourl);
                if(dominiourl != "")
                {
                  $http.get(dominiourl).then(function(res) {
                    console.log(res.data.status);
                }, function errorCallback(response) {
                    // Setup the loader
                    $ionicLoading.show({
                        content: 'Loading',
                        animation: 'fade-in',
                        showBackdrop: true,
                        maxWidth: 50,
                        showDelay: 0
                    });
                    // Set a timeout to clear loader, however you would actually call the $ionicLoading.hide(); method whenever everything is ready or loaded.
                    $timeout(function () {
                    $ionicLoading.hide();
                    alert('Url mal escrita');
                    $location.path('/admin/adminlogin');
                  }, 3000);
                });
                }else{
                  alert("Necesita setear una URL");
                  $location.path('/admin/adminlogin');
                }
                if(patente == "")
                {
                  alert("Necesita cargar la patente del movil");
                  $location.path('/admin/adminlogin');
                }
              } else {
                // Setup the loader
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 50,
                    showDelay: 0
                });

                // Set a timeout to clear loader, however you would actually call the $ionicLoading.hide(); method whenever everything is ready or loaded.
                $timeout(function () {
                $ionicLoading.hide();
                alert("URL y patente sin cargar");
                $location.path('/admin/adminlogin');
              }, 3000);

              }
          },
            function(tx, error)
            {
                console.log("Hubo un error al consultar");
            }
          );
    });
  });//ready function
}) //app.run

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $httpProvider) {

  $ionicConfigProvider.tabs.position('bottom');
  $ionicConfigProvider.backButton.text('').icon('ion-chevron-left');
  $ionicConfigProvider.views.maxCache(0);
  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })
  // setup an abstract state for the admin directive
    .state('admin', {
    url: '/admin',
    abstract: true,
    templateUrl: 'templates/admin.html'
  })
  // Each tab has its own nav history stack:
  .state('tab.dash', {
    url: '/dash',
    cache: false,
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })
    .state('tab.login', {
      url: '/login',
      views: {
        'tab-login': {
          templateUrl: 'templates/tab-login.html',
          controller: 'pLoginCtrl'
        }
      }
    })
  .state('tab.chats', {
      url: '/chats',
      views: {
        'tab-chats': {
          templateUrl: 'templates/tab-chats.html',
          controller: 'ChatsCtrl'
        }
      }
    })
    .state('tab.chat-detail', {
      url: '/chats/:chatId',
      views: {
        'tab-chats': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    })
    .state('admin.login', {
      url: '/adminlogin',
      views: {
        'admin-login': {
          templateUrl: 'templates/admin-login.html',
          controller: 'AdminCtrl'
        }
      }
    })
    .state('admin.cargardatos', {
      url: '/cargardatos',
      cache: false,
      views: {
        'admin-cargardatos': {
          templateUrl: 'templates/admin-cargardatos.html',
          controller: 'CargarDatosCtrl'
        }
      }
    })
  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/dash');
});
