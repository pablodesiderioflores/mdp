

angular.module('starter.controllers', [])
.controller('DashCtrl', function($scope,$location,$interval,$http,$ionicLoading,$timeout,Mensajes,Ot,Login,Logout) {
  $scope.mensajesView = function()
  {
    $location.path('/tab/chats');
  };
  $scope.login = function(){
    $location.path('/tab/login');
  };
  $scope.mensaje = function(){
    $location.path('/tab/chats');
  };
  $scope.msg    = Mensajes;
  $scope.ot     = Ot;
  $scope.lg     = Login;
  $scope.lgout  = Logout;
  $scope.salir  = function(){
    var link    = dominiourl +"/logout";
    var dominio = patente;
    var datos   = {domain:dominio};
    $ionicLoading.show({
          content: 'Loading',
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 50,
          showDelay: 0
        });
      $http.post(link,datos).then(function(res) {
          /*Set a timeout to clear loader, however you would
          actually call the $ionicLoading.hide();
          method whenever everything is ready or loaded.*/
          $ionicLoading.hide();
          alert(res.data.msg);
        }, function errorCallback(response) {
             $ionicLoading.hide();
             alert('Error al conectarse al servidor, intentelo de nuevo.\n Si el problema persiste comuniquese con el administrador ');
      });
  };
})
.controller('AdminCtrl', function($scope, $cordovaSQLite,$location,$state,$ionicLoading,$timeout) {
  $scope.data = {};
  $scope.cancelar = function()
  {

    $location.path('/tab/dash');
  };
  $scope.submit = function()
  {
      var usuario  = $scope.data.user;
      var password = $scope.data.pass;

      db.transaction(function(tx) {
        var sql = "select usuario, password from user where usuario ='"+ usuario +"' and password='" + password+"';";
          tx.executeSql(sql, [], function(tx, res)
          {
              if(res.rows.length > 0 )
              {
                // Setup the loader
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 50,
                    showDelay: 0
                });
                // Set a timeout to clear loader, however you would actually call the $ionicLoading.hide(); method whenever everything is ready or loaded.
                $timeout(function () {
                $ionicLoading.hide();
                alert("login correcto");
                $location.path('/admin/cargardatos');
              }, 2000);
              } else {
                alert("login incorrecto");
              }
          },
             function(tx, error)
             {
                console.log("Hubo un error al consultar");
              }
          );
      });
  };
})
.controller('CargarDatosCtrl', function($scope,$window,$location,$http,$cordovaSQLite,$ionicLoading,$timeout,$ionicHistory,$state) {
  $scope.cancelar = function()
  {
    $location.path('/tab/dash');
  };
  $scope.data   = {};
  db.transaction(function(tx){
    var consulta = "select * from movil";
    tx.executeSql(consulta,[],function(tx,res){
      if(res.rows.length > 0)
      {
        console.log("dominiourl: " + res.rows.item(0).dominiourl);
        console.log("patente: " + res.rows.item(0).patente);
        $scope.data.url     = res.rows.item(0).dominiourl; //seteamos el dominiourl de la BD a la caja de texto
        $scope.data.patente = res.rows.item(0).patente; //seteamos la patente de la BD a la caja de texto
        var patenteBD = res.rows.item(0).patente;
        /** Edición de los datos dominiourl patante en la BD SQLite **/
        $scope.editar = function()
        {
              dominiourl  = $scope.data.url; //seteo a una variable url lo que tiene la caja de texto
              patente = $scope.data.patente; //seteo a una variable pate lo que tiene la caja de texto
              if(dominiourl != '')
              {
                // Setup the loader
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 50,
                    showDelay: 0
                });
              $http.get(dominiourl).then(function(res) {
                /** Edición de los datos  */
               db.transaction(function(tx) {
                  var sql = "UPDATE movil SET dominiourl ='"+dominiourl+"', patente='"+patente+"' WHERE patente='"+patenteBD+"'";
                  //console.log(sql);
                  tx.executeSql(sql, [], function(tx, res)
                  {
                    alert("Datos insertados correctamente");
                      /*Set a timeout to clear loader, however you would
                      actually call the $ionicLoading.hide();
                      method whenever everything is ready or loaded.*/
                      $ionicLoading.hide();
                      $location.path('/tab/dash');
                  },
                    function(tx, error)
                    {
                        console.log("Hubo un error al guardar los datos en SQLite");
                    }
                  );
                  });// termina db transaction dentro de editar
               }, function errorCallback(response) {
                    // Set a timeout to clear loader, however you would actually call the $ionicLoading.hide(); method whenever everything is ready or loaded.
                    alert('Url mal escrita');
                    $ionicLoading.hide();
                    $location.path('/admin/adminlogin');

                });
          } //cierra if dominiourl    
        };//termina funcion editar
      }//termina if rows.length
    }, //termina execute SQL select * from movil
        function(tx, error)
        {
            console.log("Hubo un error al guardar los datos");
        }
    ); //termina el parentesis de consulta
  });
  $scope.data = {};
  $scope.registrar = function()
  {
    dominiourl  = $scope.data.url;
    patente     = $scope.data.patente;
    /*comprobacion url mal escrita, si va todo bien hacer insert
      y redireccionar
    */
    if(dominiourl != "")
    {
      // Setup the loader
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 50,
            showDelay: 0
        });
      $http.get(dominiourl).then(function(res) {
              /** Registro de patente y dominiourl en la base de datos */
            db.transaction(function(tx) {
            var sql = "INSERT INTO movil (dominiourl,patente) values ('"+dominiourl+"','"+patente+"');";
            //console.log(sql);
            tx.executeSql(sql, [], function(tx, res)
            {
              alert("Datos insertados correctamente");
                /*Set a timeout to clear loader, however you would
                actually call the $ionicLoading.hide();
                method whenever everything is ready or loaded.*/
                $ionicLoading.hide();
                $location.path('/tab/dash');
            },
              function(tx, error)
              {
                  console.log("Hubo un error al guardar los datos en SQLite");
              }
            );
            });
    }, function errorCallback(response) {
        // Set a timeout to clear loader, however you would actually call the $ionicLoading.hide(); method whenever everything is ready or loaded.
        alert('Url mal escrita');
        $ionicLoading.hide();
        $location.path('/admin/adminlogin');

    });
    } //cierra if dominiourl
  };//cierra scope registrar
})
.controller('pLoginCtrl', function($scope,$location, $http, $ionicLoading,$timeout,Mensajes, Ot) {
      $scope.mensajesView = function()
      {
        $location.path('/tab/chats');
      };
      $scope.msg = Mensajes;
      $scope.ot  = Ot;
      // Set the default value of inputType
      $scope.inputype = 'password';
      // Hide & show password function
      $scope.hideShowPassword = function()
      {
        if ($scope.inputype == 'password')
          $scope.inputype = 'text';
        else
          $scope.inputype = 'password';
      };
      $scope.data = {};
      $scope.procesarLogin = function(){
          var link     = dominiourl+"/login";
          var usuario  = $scope.data.user;
          var password = $scope.data.pass;
          var dominio  = patente;
          var datos    = {user: usuario, password: password, domain:dominio};
          //console.log(datos);
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 50,
                showDelay: 0
              });
          $http.post(link, datos).then(function (res){
                /*Set a timeout to clear loader, however you would
                actually call the $ionicLoading.hide();
                method whenever everything is ready or loaded.*/
                $ionicLoading.hide();
                alert(res.data.msg);
              $scope.responseUser = '';
              $location.path('/tab/chats');
          }, function errorCallback(response) {
              $ionicLoading.hide();
              $scope.responseUser = 'Error al conectarse al servidor, intentelo de nuevo.\n Si el problema persiste comuniquese con el administrador ';
        });
      };
})
.controller('ChatsCtrl', function($scope,$http, $interval, $location, Mensajes,Ot,DBA,Mdp,Login,Logout) {

  $scope.estado  = 'icon ion-ios-email-outline iconGreen';
  $scope.ot      = Ot;
  $scope.mensaje = Mensajes;
  $scope.login   = Login;
  $scope.logout  = Logout;

  $scope.mensajesView = function()
  {
    $location.path('/tab/chats');
  };
if(flagSQliteData){

  $http.get(dominiourl).then(function(res) {
      
    db.transaction(function(tx){
        
        var consulta = "SELECT mensajeria.idtipomensaje, mensajeria.idestado, mensajeria.mensaje, mensajeria.fecha, estado.estado as state, tipomensaje.descripcion FROM mensajeria LEFT JOIN tipomensaje ON mensajeria.idtipomensaje = tipomensaje.idtipomensaje LEFT JOIN estado ON mensajeria.idestado = estado.idestado WHERE fecha >= date('now','-3 day');";
        //var consulta = "select * from mensajeria";
        tx.executeSql(consulta,[],function(tx,res){
          if(res.rows.length > 0)
          {
            for (var i = 0; i < res.rows.length; i++) {
                //console.log("entre al for");
                switch (res.rows.item(i).descripcion)
                {
                  case 'login':
                    logins.push({msg: res.rows.item(i).mensaje, fecha:res.rows.item(i).fecha, tipo:res.rows.item(i).descripcion,estado:res.rows.item(i).idestado});
                    if(res.rows.item(i).idestado == 0)
                      {
                        contLogins++;
                        Login.lg = contLogins;
                      }
                    //console.log(logins);
                    break;
                  case 'logout':
                    logouts.push({msg: res.rows.item(i).mensaje, fecha:res.rows.item(i).fecha, tipo:res.rows.item(i).descripcion,estado:res.rows.item(i).idestado});
                    if(res.rows.item(i).idestado == 0)
                      {
                        contLogouts++;
                        Logout.lgout = contLogouts;
                      }
                    break;
                  case 'ot':
                    var otMsgPipe   = res.rows.item(i).mensaje;
                    var otMsgSplit = otMsgPipe.split("|");
                    //console.log(otMsgSplit[0]);
                    ots.push({id:otMsgSplit[0], desde:otMsgSplit[1],
                                  monte:otMsgSplit[2], grua:otMsgSplit[3],
                                  hac:otMsgSplit[4], hasta:otMsgSplit[5],
                                  had:otMsgSplit[6], estado: res.rows.item(i).idestado});
                    //console.log(ots);
                    if(res.rows.item(i).idestado == 0)
                      {
                        contOts++;
                        Ot.ot = contOts;
                      }

                      //console.log ('Ots: ' + Ot.ot);
                    break;
                  default:
                    mensajes.push({msg: res.rows.item(i).mensaje, fecha:res.rows.item(i).fecha, tipo:res.rows.item(i).descripcion,estado:res.rows.item(i).idestado});
                    if(res.rows.item(i).idestado == 0)
                      {
                        contMsgs++;
                        Mensajes.msj = contMsgs;
                      }
                    //console.log(mensajes);
                    break;
                }
                //console.log("ot: " + res.rows.item(i).mensaje + ', ' + res.rows.item(i).estado );
            }
          } else {
              console.log("La consulta no trajo resultados");
          }//termina if rows.length
        }, //termina execute SQL select
          function(tx, error)
          {
              console.log("Hubo un error al recuperar datos anteriores");
          }
        ); //termina el parentesis de consulta
    });

  }, function errorCallback(response) {
      // Set a timeout to clear loader, however you would actually call the $ionicLoading.hide(); method whenever everything is ready or loaded.
      alert('Debes setear bien la URL, no hay conexión con el servidor');
      $location.path('/admin/adminlogin');
  });
  flagSQliteData = false;
}

  $scope.pedirDatos = function(){
            var dominio = patente;
            var link = dominiourl+'/pending/'+dominio;
            $http.get(link).then(function (res){
              //console.log(res.data);
              res.data.msg.forEach(function (item) {
                //console.log("Tipo : " + item['type'] + " Contenido: " + item['content']);
                switch (item['type']) {
                  case 'login':
                      //console.log('Acá debería verse mensaje de login ' + item['content']);
                      logins.push({msg:item['content'], fecha:item['date'], tipo:LOGIN_CONST,estado:NO_LEIDO});
                      var loginAdd = {tipo:LOGIN_CONST, msg:item['content'], fecha:item['date'], estado:NO_LEIDO};
                      Mdp.add(loginAdd);
                      contLogins++;
                      Login.lg = contLogins;
                      //Login.lg = logins.length;
                      break;
                  case 'logout':
                      //console.log('Acá debería verse mensaje logout: ' + item['content']);
                      logouts.push({msg:item['content'], fecha:item['date'], tipo:LOGOUT_CONST, estado:NO_LEIDO});
                      var logoutAdd = {tipo:LOGOUT_CONST, msg:item['content'], fecha:item['date'], estado:NO_LEIDO};
                      Mdp.add(logoutAdd);
                      contLogouts++;
                      Logout.lgout = contLogouts;
                      //Logout.lgout = logouts.length;
                      break;
                  case 'ot':
                          //console.log('OT: ' + item['content']['id']);
                        ots.push({id:item['content']['id'], desde:item['content']['from'],
                                  monte:item['content']['monte'], grua:item['content']['grua'],
                                  hac:item['content']['loadingTime'], hasta:item['content']['to'],
                                  had:item['content']['unloadingTime'], fecha:item['content']['date'],
                                  estado:NO_LEIDO});

                        var strpipe = ots[ots.length-1].id + '|' + ots[ots.length-1].desde + '|' + ots[ots.length-1].monte + '|' + ots[ots.length-1].grua + '|' + ots[ots.length-1].hac + '|' + ots[ots.length-1].hasta + '|' + ots[ots.length-1].had;
                        otAdd       = {tipo:OT_CONST, msg:strpipe, fecha:item['date'], estado:NO_LEIDO};
                        Mdp.add(otAdd);
                        contOts++;
                        Ot.ot = contOts;
                      break;
                  default:
                      //console.log('Acá deberían verse los mensajes ' + item['content']);
                      mensajes.push({msg:item['content'], fecha:item['date'], tipo:MSJ_CONST, estado:NO_LEIDO});
                      var mensajesAdd = {tipo:MSJ_CONST, msg:item['content'], fecha:item['date'], estado:NO_LEIDO};
                      Mdp.add(mensajesAdd);
                      contMsgs++;
                      Mensajes.msj = contMsgs;
                      //console.log(Mdp.all());
                      //Mensajes.msj = mensajes.length;
                      break;
                }
              });
            });
          };
          $scope.pedirDatos();
          $interval($scope.pedirDatos, 20000);
          $scope.leido  = lectura;
          $scope.groups = [
              { name:  'Orden de Trabajo', items: ots},
              { name: 'Mensajes', items:mensajes},
              { name: 'Login', items:logins},
              { name: 'Logout', items:logouts}
          ];

  $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;

    } else {
     
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };
    
    $scope.cambiarItem = function(item)
    {
        var numeroOT = $scope.groups[0].items[item].id;
        db.transaction(function(tx) {
          var sql = "UPDATE mensajeria set idestado="+LEIDO+" WHERE idtipomensaje = 3 AND mensaje LIKE '"+ numeroOT +"|%'";
          //console.log(sql);
          tx.executeSql(sql, [], function(tx, res)
          {
              console.log("Estado actualizado correctamente a leído");
          },
             function(tx, error)
             {
                console.log("Hubo un error al actualizar el estado");
              }
          );
      });
      if($scope.groups[0].items[item].estado == 0)
      {
        contOts--;
        Ot.ot = contOts;
      }
      //console.log($scope.groups[0].items[item].estado);
      $scope.groups[0].items[item].estado = 1;
      //console.log($scope.groups[0].items[item].estado);
      $location.path('/tab/chats/'+ numeroOT);
      
    };
})
.controller('ChatDetailCtrl', function($scope, $stateParams, $http,$location,$ionicLoading,Mensajes,Ot,DBA,Mdp,Login,Logout) {
  
  $scope.data = {};
  $scope.enviarRechazo = function(){
      var url     = dominiourl+"/ot";
      var msg     = $scope.data.rechazarTextArea;
      var rechazo = 2;
      var dominio = patente;
      var numOtRe = $stateParams.chatId;
      var datoss = {domain: dominio, id: numOtRe, rechazo: rechazo, msg:msg};
      $ionicLoading.show({
        content: 'Loading',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 50,
        showDelay: 0
      });
      $http.post(url, datoss).then(function (res){
          $ionicLoading.hide();
          alert('Datos registrados correctamente');
          $location.path('/tab/chats');
          console.log(res);
      }, function errorCallback(response) {
          $ionicLoading.hide();
          alert('¡Error al conectarse al servidor, intentelo de nuevo mas tarde!');
    });
  };
  $scope.aceptar = function(){
      var link     = dominiourl+"/ot";
      var dominio  = patente;
      var numOt    = $stateParams.chatId;
      var aceptar  = 1;
      var datos    = {domain: dominio, id: numOt, aceptar:aceptar};
      //console.log(datos);
      $ionicLoading.show({
        content: 'Loading',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 50,
        showDelay: 0
      });
      $http.post(link, datos).then(function (res){
          $ionicLoading.hide();
          alert('Datos registrados correctamente');
          $location.path('/tab/chats');
          //console.log(res);
      }, function errorCallback(response) {
          $ionicLoading.hide();
          $scope.responseUser = '¡Error al conectarse al servidor, intentelo de nuevo mas tarde!';
    });
  };
  function population (argument) {
    for (var i = 0; i < ots.length; i++) {
        if (ots[i]['id'] === $stateParams.chatId){
          //console.log('Entré al if ...');
          //console.log('Soy ots ...' + ots[i]);
          return ots[i];
        }
      }
  }
  $scope.ot = population();
})
.controller('AccountCtrl', function($scope, Mensajes, Ot) {
  $scope.msg = Mensajes;
  $scope.ot  = Ot;

  $scope.settings = {
    enableFriends: true
  };
});
